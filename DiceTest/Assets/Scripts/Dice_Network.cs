﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Dice_Network : MonoBehaviour {
    public float maxRollTime = 0.45f; // Max RollTime should be around 1/2 second, so just to be sure set it a little lower
    public float delay = 0.1f; // Delay before we know what the outcome should be
    public bool continuousSimulation = false; // If true, dice will be thrown automatically

    private int wantedOutcome;
    private Vector3 limitsMin;
    private Vector3 limitsMax;
    private Vector3 center;
    private float endDiceAnimationHeight = 0.6813511f;
    private Rigidbody rb;
    private float throwForce = 10f;
    private bool isThrown = false;
    private bool outcomeKnown = false;
    private bool finishingRoll = false;
    private float currentRollTime;
    private float finishRollStartTime;

    private Vector3 endPosition;
    private Quaternion endRotation;

    void Start()
    {
        limitsMin = new Vector3(0.5f, 0.5f, -4.5f);
        limitsMax = new Vector3(5.0f, 3.0f, 4.5f);
        center = new Vector3((limitsMin.x + limitsMax.x) / 2,
                             0.8f,
                             (limitsMin.z + limitsMax.z) / 2);

        rb = GetComponent<Rigidbody>();
    }

    private void OnValidate()
    {
        if (maxRollTime <= 0) { maxRollTime = 0.45f; }
    }

    private void Update()
    {
        currentRollTime += Time.deltaTime;

        // Simulate network delay and randomize wanted outcome
        if (currentRollTime >= delay && isThrown && !outcomeKnown)
        {
            // Randomize outcome
            wantedOutcome = Random.Range(1, 7);
            Debug.Log("Random Outcome: " + wantedOutcome);
            outcomeKnown = true;
            // Save the time we found the outcome
            finishRollStartTime = currentRollTime;
            // Find end rotation and position definened by the outcome and finish the roll to fit it
            FindEndPoints();
            finishingRoll = true;
        }

        // Finish the roll by lerping from current position/rotation to end points
        if (finishingRoll)
        {
            float t = Mathf.Clamp01((currentRollTime - finishRollStartTime) / (maxRollTime - finishRollStartTime));
            transform.position = Vector3.Lerp(transform.position, endPosition, t);
            transform.rotation = Quaternion.Lerp(transform.rotation, endRotation, t);

            // Finish the throw if we reach the end point
            if (t == 1)
            {
                isThrown = false;
            }
        }

        // Throw the dice on left mouse button or if continuousSimulation is true, throw automatically
        if ((Input.GetMouseButtonDown(0) || continuousSimulation) && !isThrown)
        {
            // Reset some bool values
            isThrown = true;
            outcomeKnown = false;
            finishingRoll = false;
            currentRollTime = 0f;
            // Throw
            Throw();
        }
    }

    /// <summary>
    /// Get random start position above the playing board. Position will be within the set limits.
    /// </summary>
    Vector3 GetRandomStartPosition()
    {
        return new Vector3(Random.Range(limitsMin.x, limitsMax.x),
                            Random.Range(limitsMin.y, limitsMax.y),
                            Random.Range(limitsMin.z, limitsMax.z));
    }

    /// <summary>
    /// Get directon in which the dice will be thrown. Currently just thrown in the direction a little above center.
    /// </summary>
    Vector3 GetThrowDirection()
    {
        return (center - transform.position).normalized;
    }

    /// <summary>
    /// Uses random forces to simulate dice rolling i.e. the beginning of the roll.
    /// </summary>
    void Throw()
    {
        // Set rigidbody to dynamic
        rb.isKinematic = false;
        // Get random start position and set it
        Vector3 startPosition = GetRandomStartPosition();
        transform.position = startPosition;
        // Reset the rotation (or get random rotation in the future)
        Quaternion startRotation = Quaternion.Euler(Vector3.zero);
        transform.rotation = startRotation;
        // Reset riidbody velocity
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        // Get throw direction
        Vector3 throwDirection = GetThrowDirection();

        // Throw the dice by applying forces the the rigidbody
        rb.AddForce(throwDirection * throwForce, ForceMode.Impulse);
        rb.AddTorque(throwDirection * throwForce, ForceMode.Impulse);       
    }

    /// <summary>
    /// Finds end position and rotation to fit the wanted outcome.
    /// </summary>
    void FindEndPoints()
    {     
        // Cache start values
        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.rotation;
        Vector3 startVelocity = rb.velocity;
        Vector3 startAngularVelocity = rb.angularVelocity;

        float distance = 3f; // Distance in the velocity direction in which we calculate end position, so it looks like the dice is moving further

        // Calculate and assign location and rotation of endpoint
        endPosition = transform.position + startVelocity.normalized * distance;
        endPosition.y = endDiceAnimationHeight; // Assign Y value so we dont overshoot the playing board

        // Calculate end rotation
        // Currently end rotation is calculated kinda weird, so it really looks like loaded dice. Change this for better visualization.
        endRotation = Quaternion.identity * Quaternion.FromToRotation(GetDiceResultDirectionGlobal(wantedOutcome), Vector3.up);

        // Set rigidbody to kinematic so its not affected by forces anymore. (or maybe just set gravity to 0 if there aren't any other)
        rb.isKinematic = true;
    }

    /// <summary>
    /// Returns directional vector which points up based on the result (that will be shown)
    /// </summary>
    Vector3 GetDiceResultDirectionGlobal(int result)
    {
        switch (result)
        {
            case 1:
                return Vector3.back;
            case 2:
                return Vector3.right;
            case 3:
                return Vector3.up;
            case 4:
                return Vector3.down;
            case 5:
                return Vector3.left;
            case 6:
                return Vector3.forward;
        }

        return Vector3.zero;
    }
}
