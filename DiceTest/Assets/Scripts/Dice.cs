﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Dice : MonoBehaviour {

    [Range(1, 6)]
    public int wantedOutcome = 5; // What should be the dice roll result
    public float maxRollTime = 0.45f; // Max RollTime should be around 1/2 second, so just to be sure set it a little lower
    public bool continuousSimulation = false; // If true, dice will be thrown automatically
    public bool useCasualThrow = false; // If true, there will be no time constraint and physics will just do its job unitywise
    public GameObject gfx; // Actual dice graphics (separated for better rotational and force calculations)

    private Vector3 limitsMin;
    private Vector3 limitsMax;
    private Vector3 center;

    private Rigidbody rb;
    private float throwForce = 10f;
    private bool isThrown = false;
    private float currentRollTime;

    List<TimePoint> timePoints; // List of current transform data from roll simulation, will be iterated over.

	void Start () {
        // Set Limits
        limitsMin = new Vector3(0.5f, 0.5f, -4.5f);
        limitsMax = new Vector3(5.0f, 3.0f, 4.5f);
        // Calculate center, make the Y a little higher so we don't roll exactly into center but a little above it
        center = new Vector3((limitsMin.x + limitsMax.x) / 2,
                             0.8f, 
                             (limitsMin.z + limitsMax.z) / 2);

        rb = GetComponent<Rigidbody>();        

        timePoints = new List<TimePoint>();
    }

    private void OnValidate()
    {
        if (wantedOutcome > 6) { wantedOutcome = 6; }
        if (wantedOutcome < 1) { wantedOutcome = 1; }
        if (maxRollTime <= 0)  { maxRollTime = 0.45f; }
    }

    void Update () {

        // Throw the dice on left mouse button or if continuousSimulation is true, throw automatically
        if ((Input.GetMouseButtonDown(0) || continuousSimulation ) && !isThrown)
        {
            // Reset some values
            isThrown = true;
            currentRollTime = 0f;
            // Throw the dice!
            Throw();
        }

        // If its thrown and the simulation ran (timepoints have some data), iterate over them and visualize the throw
        if (isThrown && !useCasualThrow && timePoints.Count > 0)
        {
            // Get the time we want to show, while restricting it by maxRollTime
            float t = Mathf.Clamp01(currentRollTime / maxRollTime);
            // Lerp position and rotation based on that time
            transform.position = Vector3.Lerp(transform.position, timePoints[Time01ToTimePointIndex(t)].position,  t);
            transform.rotation = Quaternion.Lerp(transform.rotation, timePoints[Time01ToTimePointIndex(t)].rotation, t);
            currentRollTime += Time.deltaTime;

            // Finish the throw if iteration is over
            if (t == 1)
            {
                isThrown = false;
                timePoints.Clear();
                Debug.Log("Roll Time: " + currentRollTime);
            }
        }

        // Just in case something unpredictible happens
        if (isThrown && timePoints.Count == 0 && !useCasualThrow)
        {
            isThrown = false;
            Debug.Log("Roll Time: " + currentRollTime);
        }

        // If casual throw is on, rigidbody will be dynamic so we check for the end by checking the rigidbody itself
        if (rb.IsSleeping() && isThrown && useCasualThrow)
        {
            Debug.Log("Roll Time: " + currentRollTime);
            isThrown = false;
        }
    }

    /// <summary>
    /// Returns nearest index from timepoints based on input time
    /// </summary>
    int Time01ToTimePointIndex(float t)
    {
        return Mathf.RoundToInt((timePoints.Count - 1) * t);
    }

    /// <summary>
    /// Get random start position above the playing board. Position will be within the set limits.
    /// </summary>
    Vector3 GetRandomStartPosition()
    {
        return new Vector3( Random.Range(limitsMin.x, limitsMax.x), 
                            Random.Range(limitsMin.y, limitsMax.y), 
                            Random.Range(limitsMin.z, limitsMax.z));
    }

    /// <summary>
    /// Get directon in which the dice will be thrown. Currently just thrown in the direction a little above center.
    /// </summary>
    Vector3 GetThrowDirection()
    {
        return (center - transform.position).normalized;
    }

    /// <summary>
    /// Uses random forces to simulate dice rolling, then runs the simulation on the outcome and corrects the start rotation to fit our wanted outcome.
    /// </summary>
    void Throw()
    {
        SimulationInfo result = new SimulationInfo();
        // Try to get a result in the simulation
        int maxTries = 30; // It takes 2 tries in extreme cases, so its 1 try most of the time
        for (int i = 0; i < maxTries; i++)
        {
            timePoints.Clear();
            // Set rigidbody to dynamic so it is affected by forces
            rb.isKinematic = false;
            // Get random start position and rotation and set them
            Vector3 startPosition = GetRandomStartPosition();
            transform.position = startPosition;
            Quaternion startRotation = Quaternion.Euler(Vector3.zero);
            gfx.transform.rotation = Quaternion.Euler(Vector3.zero); // reset the GFX so it fits the parent rotation
            transform.rotation = startRotation;
            // Reset the rigidbody velocity
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            // Get throw direction
            Vector3 throwDirection = GetThrowDirection();     

            // Simulate the throw and return result
            result = SimulateThrow(throwDirection);

            // Correct the GFX rotation to fit the result - basically rotate it so we get our wanted outcome
            CorrectGfxRotationForWantedOutcome(result.correction);

            // Set rigidbody to kinematic so its not affected by forces and we can iterate over simulation data
            rb.isKinematic = true;

            // If the result is something we can use, break. Else run the simulation again.
            if (result.outcome != 0)
            {
                Debug.Log("Tries: " + (i+1).ToString());
                break;
            }
        }

        // If there is no time contraint, we just let the physics do its job
        if (useCasualThrow)
        {
            rb.isKinematic = false;
            rb.AddForce(result.throwDirection * throwForce, ForceMode.Impulse);
            rb.AddTorque(result.throwDirection * throwForce, ForceMode.Impulse);
        }
    }

    /// <summary>
    /// Rotates GFX object so the wanted outcome is shown at the of the roll. Correction is calculated in simulation
    /// </summary>
    void CorrectGfxRotationForWantedOutcome(Quaternion correction)
    {
        gfx.transform.rotation = correction;
    }

    /// <summary>
    /// Simulation of the current throw. Returns Simulationinfo with all of the data that should be needed visualize it or reproduce it.
    /// </summary>
    SimulationInfo SimulateThrow(Vector3 throwDirection)
    {
        SimulationInfo result = new SimulationInfo(); 

        // Turn of autoSimulation, so we can simulate it manually in one swoop and not wait for unity FixedUpdate.
        Physics.autoSimulation = false;

        // Get some start values
        Vector3 startPosition = transform.position;
        Quaternion startRotation = transform.rotation;
        Vector3 startVelocity = rb.velocity;
        Vector3 startAngularVelocity = rb.angularVelocity;      

        // Throw the Dice!
        rb.AddForce(throwDirection * throwForce, ForceMode.Impulse);
        rb.AddTorque(throwDirection * throwForce, ForceMode.Impulse);

        // Wait some time for the roll to finish and then record the result
        int iterations = 1000; // Basically a safety net if the dice falls out of the board and keeps falling.
        float step = Time.fixedDeltaTime; // Physics simulation step
        for (int i = 0; i < iterations; i++)
        {
            // Make one simulation step
            Physics.Simulate(step);
            // Add the current position/rotation to list so we can iterate over it later in visualization
            timePoints.Add(new TimePoint(transform.position, transform.rotation));

            // If rigidbody is sleeping, then the roll is finished
            if (rb.IsSleeping())
            {
                // Get the dice outcome
                int outcome = GetDiceResult(this.transform);

                //Debug.Log("Result: " + outcome + "\nTime: " + (step * i).ToString("#0.0000") + " || Iterations: " + i);

                // Save simulation data to struct for later use
                result.outcome = outcome;
                result.rollTime = step * i;
                result.iterations = i;
                result.startPosition = startPosition;
                result.endPosition = transform.position;
                result.throwDirection = throwDirection;
                result.startRotation = startRotation;
                result.endRotation = transform.rotation;
                result.startVelocity = startVelocity;
                result.startAngularVelocity = startAngularVelocity;
                result.timePoints = timePoints;

                // Calculate correction, which will be applied to GF rotation so we get our loaded dice
                Vector3 currentResultDirection = GetDiceResultDirectionGlobal(outcome);
                Vector3 wantedResultDirection = GetDiceResultDirectionGlobal(wantedOutcome);
                result.correction = Quaternion.FromToRotation(wantedResultDirection, currentResultDirection) ;

                break;
            }
        }

        // Reset to original values before simulation
        transform.position = startPosition;
        transform.rotation = startRotation;
        rb.velocity = startVelocity;
        rb.angularVelocity = startAngularVelocity;

        // And reset the auto simulation so unit can continue working for us
        Physics.autoSimulation = true;       

        return result;
    }    

    /// <summary>
    /// Returns the current outcome of the dice roll.
    /// </summary>
    int GetDiceResult(Transform transformObj)
    {
        float eps = 0.0002f;
        if (Vector3.Dot(Vector3.up, transformObj.up) >= 1 - eps) { return 3; }
        if (Vector3.Dot(Vector3.up, -transformObj.up) >= 1 - eps) { return 4; }
        if (Vector3.Dot(Vector3.up, transformObj.right) >= 1 - eps) { return 2; }
        if (Vector3.Dot(Vector3.up, -transformObj.right) >= 1 - eps) { return 5; }
        if (Vector3.Dot(Vector3.up, transformObj.forward) >= 1 - eps) { return 6; }
        if (Vector3.Dot(Vector3.up, -transformObj.forward) >= 1 - eps) { return 1; }
        return 0;
    }

    /// <summary>
    /// Returns directional vector which points up based on the result (that will be shown)
    /// </summary>
    Vector3 GetDiceResultDirectionGlobal(int result)
    {
        switch (result)
        {
            case 1:
                return Vector3.back;
            case 2:
                return Vector3.right;
            case 3:
                return Vector3.up;
            case 4:
                return Vector3.down;
            case 5:
                return Vector3.left;
            case 6:
                return Vector3.forward;

        }

        return Vector3.zero;
    }
}

/// <summary>
/// Stores data about the dice roll simulation.
/// </summary>
struct SimulationInfo
{
    public float rollTime;
    public int outcome;
    public int iterations;
    public Quaternion correction;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public Vector3 throwDirection;
    public Quaternion startRotation;
    public Quaternion endRotation;
    public Vector3 startVelocity;
    public Vector3 startAngularVelocity;
    public List<TimePoint> timePoints;
}

/// <summary>
/// Stores transform data at on point in time.
/// </summary>
struct TimePoint
{
    public Vector3 position;
    public Quaternion rotation;

    public TimePoint(Vector3 _position, Quaternion _rotation)
    {
        this.position = _position;
        this.rotation = _rotation;
    }
}
    

